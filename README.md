# Construction des images docker Odoo e-COSI

## Objectifs

Nous construisons des images custo e-COSI pour les raisons suivantes :
  * pas d'images EN dispo par l'éditeur
  * pour les CE la fréquence en mensuelle, nous souhaitons de l'hebdo pour les bugfix


## Principe

L'image CE officielle est contruite à partir du package .deb mensuel.

Pour faire notre propre image, nous nous inspirons du package (dépendances) pour tout construire via lde Dockerfile.
L'entrypoint est repris de celui d'Odoo.

Nous ajoutons les dépendances python nécessaires à certains addon via les `python_extra_requirements`

## Fonctionnement

Le playbook est fait pour enchaîner la construction des images CE et EN d'une version d'Odoo. L'image EN se base l'image CE donc pas de build séparé.

Le build avant de débuter détruire les image présentes ayant les tags suivants :
  * VERSION-<date du jour>
  * VERSION
  * latest (si pertinent)

Il n'y a pas de nettoyage des versions précédentes VERSION-<date antérieur>

Les versions d'Odoo sont gérées via les host dans l'inventory. Pour ne construire qu'une version d'images, utiliser les limitations de hosts.
Les hosts sont de la forme `docker-runner-nn` ou nn est le numéro de version Odoo (ex: 9, 12).

Pour ne construire que la V12 limiter à : `*12*`

## Structure AWX

Deux projets, basés sur deux repo sont nécessaire car notre version actuelle d'AWX ne permet pas l'utilisation du vault dans les inventory.

Nous avons donc un projet pour le playbook et un pour l'inventory. Les secrets cryptés avec Vault sont donc dans le projet du playbook.

## Exécution automatique

Elle est gérée par le scheduler AWX.